#include "sampleitem.h"

void Sample::propagate(Item *item) {

    if(item->is_stopped()) return;

    double x_it = item->get_x()+dt*item->get_vx();
    double y_it = item->get_y()+dt*item->get_vy();
    if(item->get_area() > 0) {
        double x = areas[item->get_area()]->get_x();
        double y = areas[item->get_area()]->get_y();
        double r = areas[item->get_area()]->get_radius();

        double d2 = (x_it-x)*(x_it-x)+(y_it-y)*(y_it-y);

        if(d2 >= r*r) {
            double vx= item->get_vx();
            double vy= item->get_vy();

            item->set_vx(-vx);
            item->set_vy(-vy);
        }
        else {
            item->set_x(x_it);
            item->set_y(y_it);

        }
    }
    else {


        double x_dest = areas[item->get_destination()]->get_x();
        double y_dest = areas[item->get_destination()]->get_y();

        double d2 =(x_it-x_dest)*(x_it-x_dest) + (y_it-y_it)*(y_it-y_it);

        if(sqrt(d2) <= areas[item->get_destination()]->get_radius())
        {
            item->set_vx(0);
            item->set_vy(0);

            item->set_area(item->get_destination());
            item->set_destination(item->get_destination());

        }
        else {
            item->set_x(x_it);
            item->set_y(y_it);

        }

    }



}



bool Sample::interact(Item *item1, Item *item2) {

    double  x1 = item1->get_x();
    double y1 = item1->get_y();

    double x2 = item2->get_x();
    double y2 = item2->get_y();

    double d2 = (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);

    return sqrt(d2) <= r_max ? true : false;
}


void Sample::diagnose(Item* item) {
    if(item->get_status()==1) {
        item->set_status(3);
        item->set_stop(true);
        item->set_vx(0);
        item->set_vy(0);
    }
}


void Sample::initialize_items(std::vector<Item*> items) {

    for(auto it : items) {

        if(rnd.rnd01() < ill_initial_ratio) {

            ill_items.push_back(it);
            it->set_status(1);

        }

        else {

            it->set_status(0);

        }


        double area_r = areas[it->get_area()]->get_radius();
        double area_x = areas[it->get_area()]->get_x();
        double area_y = areas[it->get_area()]->get_y();


        it->set_x(area_x+(rnd.rnd01()-0.5)*2*area_r);
        it->set_y(area_y+(rnd.rnd01()-0.5)*2*area_r);

        if(rnd.rnd01() > 0.5) {
            it->set_stop(true);
            continue;
        }
        else {
            it->set_stop(false);
            it->set_vx(rnd.rndN(sqrt(5),1));
            it->set_vy(rnd.rndN(sqrt(5),1));
        }

    }
}

void Sample::make_step() {
    for(auto &item : items) {
        if(item->get_status()!=3 && item->is_stopped()) {
            if(rnd.rnd01() <1-stop_threshold) {
                item->set_vx(rnd.rndN(sqrt(5),1));
                item->set_vy(rnd.rndN(sqrt(5),1));
            }
            item->set_stop(false);
        }

        if(item->is_stopped() == false) {
            if(rnd.rnd01() <stop_threshold) {
                item->set_vx(0);
                item->set_vy(0);
                item->set_stop(true);
            }

        }

        propagate(item);



    }

    for(size_t i=1; i<areas.size(); i++) {
        auto itemss = areas[i]->get_items();
        for(size_t p=0; p < itemss.size(); p++) {
            for(size_t q=0; q <ill_items.size(); q++) {
                if(ill_items[q] == itemss[p]) continue;
                if(interact(itemss[p],ill_items[q])==false) continue;
                size_t p_status = itemss[p]->get_status();
                size_t q_status = ill_items[q]->get_status();
                itemss[p]->set_met_item(ill_items[q]);
                if(p_status ==0 && q_status == 1 && rnd.rnd01() >= ill_threshold) {
                    itemss[p]->set_status(1);

                    ill_items.push_back(itemss[p]);
                }

            }
        }


    }

    std::vector<Item*> ill_temp;

    for(size_t i=0U; i<ill_items.size(); i++) {
        ill_items[i]->set_illness_time(dt);

        if(ill_items[i]->get_illness_time() >= 24*14) {

            ill_items[i]->set_status(2);
            ill_items[i]->set_illness_time(0);
        }
        else {
            ill_temp.push_back(ill_items[i]);
        }
    }

    ill_items = ill_temp;
    time+=dt;
}

void Sample::add_center(double x, double y, double r, size_t N) {

    Area* a = new Area();
    a->set_x(x);
    a->set_y(y);
    a->set_radius(r);
    a->set_id(areas.size());
    areas.push_back(a);
    for(size_t i=0; i< N; i++) {
        Item* it = new Item();
        it->set_area(a->get_id());
        a->set_item(it);
        it->set_id(items.size());
        items.push_back(it);
    }

    initialize_items(a->get_items());

}

Sample::Sample(double r,double r_m,double d_t,double iir,double ill_t,double stop_th) {
    r_max =r_m;
    dt= d_t;
    ill_initial_ratio = iir;
    add_center(0,0,r,0);
    ill_threshold=ill_t;
    stop_threshold=stop_th;
}



void Sample::print(size_t mode) const {
    if(mode==0U) {
        for(auto item : items)
            item->print();
    }

    if(mode==1U) {
// for(auto item : ill_items)
//  item->print();
        std::cout<<time<<" "<<ill_items.size()<<std::endl;
    }


}
