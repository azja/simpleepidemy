
#ifndef ITEM_H
#define ITEM_H
#include <iostream>
#include <math.h>
#include <vector>
#include<set>

class Item {

    size_t id;
    double v_x;
    double v_y;

    double x;
    double y;

    std::set<Item*> met_items;
    int area_id; // > 0 in area; 0 is travelling
    int  destination;
    double suscebility;
    bool stopped;
    size_t status; // 0 healthy; 1 ill; 2 recovered; 3 diagnosed positevly; 4 dead

    double illness_time;

public:

    Item()=default;

    /*
     getters
    */
    double get_vx() const;
    double get_vy() const;

    double get_x() const;
    double get_y() const;

    int get_area() const;
    int get_destination() const;
    double get_susc() const;
    size_t  get_status() const;
    double get_illness_time() const;
    Item* get_met_item(size_t);
    bool is_stopped() const;
    /*
     setters
    */


    void set_vx(double);
    void set_vy(double);

    void set_x(double);
    void set_y(double);

    void set_id(size_t);
    void set_area(int);
    void set_destination(int);
    void set_susc(double);
    void  set_status(size_t);
    void set_illness_time(double);
    void set_met_item(Item*);
    void set_stop(bool);


    void print() const;


};

#endif
