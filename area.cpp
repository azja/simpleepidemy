#include "area.h"

size_t Area::get_id() const {
    return id;
}

size_t Area::get_x() const {
    return x;
}

double Area::get_y() const {
    return y;
}
double Area::get_radius() const {
    return radius;
}


std::vector<Item*> Area::get_items() {
    return items;
}

Item* Area::get_item(size_t id) {
    return items[id];
}


/*
 Setters
*/

void Area::set_id(size_t arg) {
    id = arg;
}

void Area::set_x(double arg) {
    x = arg;
}

void Area::set_y(double arg) {
    y = arg;
}

void Area::set_radius(double arg) {
    radius = arg;
}

void Area::set_item(Item* it) {
    items.push_back(it);
}



