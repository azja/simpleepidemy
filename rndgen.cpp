#include "rndgen.h"

Rnd::Rnd() {
    gen=std::mt19937(rd());
}

double Rnd::rnd01() {
    std::uniform_real_distribution<> dis(0,1);
    return dis(gen);
}

double Rnd::rndN(double mean,double dev) {
    std::normal_distribution<> dis(mean,dev);
    return dis(gen);
}
