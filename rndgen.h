#ifndef RND_H
#define RND_H
#include <random>
#include <iostream>

class Rnd {
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen;


public:
    Rnd();

    double   rnd01();
    double   rndN(double mean, double dev);

};

#endif
