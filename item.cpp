#include "item.h"

/*
 getters
*/
double Item::get_vx() const {
    return v_x;
}

double Item::get_vy() const {
    return v_y;
}

double Item::get_x() const {
    return x;
}

double Item::get_y() const {
    return y;
}

int Item::get_area() const {
    return area_id;
}

int Item::get_destination() const {
    return destination;
}

double Item::get_susc() const {
    return suscebility;
}

size_t  Item::get_status() const {
    return status;
}

double Item::get_illness_time() const {
    return illness_time;
}

Item* Item::Item::get_met_item(size_t id) {
    std::set<Item*>::iterator it = met_items.begin();
    std::advance(it, id);

    return *it;
}

bool Item::is_stopped() const {
    return stopped;
}
/*
 setters
*/


void Item::set_vx(double arg) {
    v_x = arg;
}
void Item::set_vy(double arg) {
    v_y = arg;
}

void Item::set_x(double arg) {
    x = arg;
}

void Item::set_y(double arg) {
    y = arg;
}

void Item::set_area(int arg) {
    area_id = arg;
}
void Item::set_destination(int arg) {
    destination = arg;
}

void Item::set_susc(double arg) {
    suscebility = arg;
}
void Item::set_status(size_t arg) {
    status = arg;
    if(status == 2 || status == 4)
        met_items.clear();
}

void Item::set_illness_time(double arg) {
    illness_time+=arg;
}

void Item::set_met_item(Item* it) {
    met_items.insert(it);
}

void Item::set_stop(bool s) {
    stopped = s;
}

void Item::print() const {
    std::cout<<id<<" "<<x<<" "<<y<<status<<std::endl;
}


void Item::set_id(size_t idx) {
    id =idx;
}