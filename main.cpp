#include "sampleitem.h"

int main() {

    Sample sample(300,0.00005,1.0/3600,0.002,0.95,0);
    sample.add_center(0,0,2.5,1000);
    for(size_t i=0; i<1e+9; i++) {
        sample.make_step();
        if(i%3600==0)
            sample.print(1);
    }

    return 0;
};
