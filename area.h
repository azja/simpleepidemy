#ifndef AREA_H
#define AREA_H
#include<iostream>
#include "item.h"
#include<vector>
#include<list>
#include <random>

class Area {
    size_t id;
    double x;
    double y;
    double radius;
    std::vector<Item*> items;

public:

    Area()=default;

    /*
     Getters
    */

    size_t get_id() const;
    size_t get_x() const;
    double get_y() const;
    double get_radius() const;
    std::vector<Item*> get_items();
    Item* get_item(size_t id);



    /*
     Setters
    */

    void set_id(size_t);
    void set_x(double);
    void set_y(double);
    void set_radius(double);
    void set_item(Item*);







};


#endif
