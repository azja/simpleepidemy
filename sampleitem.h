#ifndef SAMPLEITEM_H
#define SAMPLEITEM_H
#include <iostream>
#include "area.h"
#include "rndgen.h"
class Sample {

    std::vector<Item*> items;
    std::vector<Item*> ill_items;
    std::vector<Item*> dead_items;
    std::vector<Item*> recovered_items;
    std::vector<Area*> areas;

    /*
    TO DO: more cities (areas), now not important
    */
    double max_velocity_inside;
    double max_velocity_outside;
    double min_velocity_outside;

    /*
    Time needed for infected item to recover in [h] (typically 14 days)
    */
    double recovery_time_threshold;

    /*
    Will be (or is ?) responsible to describe immunologic resistance (random(0,1) > ill_threshold => is infected after interaction will infected item)
    */
    double ill_threshold;

    /*
    Global time of simulation
    */

    double time;

    /*
    Initial ratio of infected items - input parameter
    */
    double ill_initial_ratio; 

    /*
    Random number generator
    */
    Rnd rnd;
    
    /*
    Maximal radius in [km] for which item interact with other item (typicall 1.0e-4 km)
    */
    double r_max;

    /*
    Time step in [h] (typically 1/3600 = 1s)
    */
    double dt;
        
    /*
    random number (0,1) > stop_threshold - item is stopped and cannot interact
    */
    
    double stop_threshold;
    void propagate(Item *item);
    bool interact(Item *item1, Item *item2);

    void initialize_items(std::vector<Item*> items);

    void diagnose(Item*);
public:
    void make_step();
    Sample(double r,double r_m,double d_t,double iir,double ill_t,double stop_th);
    void print(size_t mode) const; //0-all, 1-ill

    void add_center(double x, double y, double r, size_t N);
};

#endif
